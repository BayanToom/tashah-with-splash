package com.example.user.mytabs.Activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import com.example.user.imageslider.TopFiveSlidingAdapter
import com.example.user.mytabs.Adapters.MyPagerAdapter
import com.example.user.mytabs.Models.ImageModel
import com.example.user.mytabs.R
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    var firebaseStore: FirebaseFirestore = FirebaseFirestore.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val fragmentAdapter = MyPagerAdapter(supportFragmentManager)
        viewpager_main.adapter = fragmentAdapter
        viewpager_main.setOnTouchListener(object : View.OnTouchListener{
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                return true
            }

        })

        viewpager_main.setPageSwapEnabled(false)

//        val tab = findViewById<>()
        tabs_main.post {
            tabs_main.setupWithViewPager(viewpager_main)
        }



        tabs_main.setOnTabSelectedListener(object : TabLayout.BaseOnTabSelectedListener<TabLayout.Tab>{
            override fun onTabReselected(p0: TabLayout.Tab?) {

            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                Toast.makeText(applicationContext,"Tab Selected!", Toast.LENGTH_SHORT).show()

            }

        })
//        tabs_main.setupWithViewPager(viewpager_main)

        //arraylist
//        var places = ArrayList<Place>()
//
//        places.add(
//            Place(
//                "Ward Resturant",
//                "Discription",
//                "https://media-cdn.tripadvisor.com/media/photo-s/05/b8/80/25/ward.jpg"
//            )
//        )
//        places.add(
//            Place(
//                "InDoor Cafe",
//                "Discription",
//
//                "http://imgsrv2.pxdrive.com/pics/norm/281449.jpg"
//            )
//        )
//        places.add(
//            Place(
//                "java u",
//                "Discription",
//                "https://pbs.twimg.com/media/B-xbaBjUMAAVGAn.jpg"
//            )
//        )
//
//        places.add(
//            Place(
//                "xxxxxxxx",
//                "Discription",
//                "https://www.fakenamegenerator.com/images/sil-female.png"
//            )
//        )
        //firebase
//        val settings = FirebaseFirestoreSettings.Builder()
//            .setTimestampsInSnapshotsEnabled(true)
//            .build()
//        firebaseStore.setFirestoreSettings(settings)
//        var name: String = mRestName.text.toString().trim()
//        val values = HashMap<String, ArrayList<Place>>()
//        values.put("place_name", places)
//
//        firebaseStore.collection("Places")
//            .add(values as Map<String, Any>)
//            .addOnSuccessListener { documentReference ->
//
//            }
    }

}

