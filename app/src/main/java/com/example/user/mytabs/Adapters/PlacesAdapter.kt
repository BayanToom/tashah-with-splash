package com.example.user.mytabs.Adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.user.mytabs.Models.Place
import com.example.user.mytabs.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_temp.view.*


class PlacesAdapter (var context: Context, var places: ArrayList<Place>) : BaseAdapter() {


    override fun getItem(position: Int): Place {
        return places.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return places.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {


        var mainView: View
        var viewHolder: ViewHolder
        if (convertView == null) {

            var layoutInflater = LayoutInflater.from(context);
            mainView = layoutInflater.inflate(R.layout.item_temp, parent, false)
            viewHolder = ViewHolder(
                mainView.mRestName,
                mainView.mDescriotion,
                mainView.mImage as ImageView
            )
            Log.d("MYAPP", "We are here for " + position)

            mainView.tag = viewHolder
        } else {
            mainView = convertView
            viewHolder = convertView.tag as ViewHolder
        }


        var place: Place = getItem(position)
        viewHolder.mRestName.text = place.placeName
        viewHolder.mDescriotion.text = place.discription

        Picasso.get().load(place.placePicture).into(viewHolder.mImage);

        return mainView
    }

    private class ViewHolder(var mRestName: TextView, var mDescriotion: TextView, var mImage: ImageView)
}
