package com.example.user.mytabs.Adapters

import android.content.Context
import android.os.Parcelable
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.user.mytabs.Models.QuestionModel
import com.example.user.mytabs.R

class QuestionsSlidingAdapter(val context: Context, val questionModelArrayList: ArrayList<QuestionModel>) :
    PagerAdapter() {
    var inflater: LayoutInflater

    init {
        inflater = LayoutInflater.from(context)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return questionModelArrayList.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val questionLayout = inflater.inflate(R.layout.sliding_question_layout, view, false)!!

        val textView = questionLayout
            .findViewById(R.id.textViewQuestion) as TextView


        textView.text= questionModelArrayList.get(position).image
        //Picasso.get().load(questionModelArrayList.get(position).image).into(textView)

        view.addView(questionLayout, 0)

        return questionLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }
}
