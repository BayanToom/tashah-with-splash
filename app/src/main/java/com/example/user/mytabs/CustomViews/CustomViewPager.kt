package com.example.user.mytabs.CustomViews

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent


/**
 * @Description Custom ViewPager Class to disable Swap functionality
 * @Author  Bayan
 * @year 2018
 */

class CustomViewPager : ViewPager {

    /*
        We create a constructor matching parent that takes:
            @Attr: Context, AttributeSet
        Which let us take Context ( What happend in the program so far),
            AttributeSet ( the Layout functionality like : layout_height )
        And then we call the super for each of them.
        Then we set the Enabled to true
     */

    constructor(context: Context,attr : AttributeSet) : super(context, attr) {
        this.isEnabled = true
    }

    /*
        On TouchEvents ( Like a Regular touch )
            We Check to see if the touch is enabled or not
                then we return the parent functionality for the touch
     */

    override fun onTouchEvent(ev: MotionEvent?): Boolean {
        if(this.isEnabled)
            return super.onTouchEvent(ev)

        return false
    }

    /*
        OnInterceptTouchEvent ( Like Swap )
            We Check if the touch is enabled or not then we return the father functionality for the event
     */


    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        if(this.isEnabled)
            return super.onInterceptTouchEvent(ev)
        return false
    }

    /*
        setPageSwapEnabled
            is For enabling or disabling the swap functionality
     */
    public fun setPageSwapEnabled (enabled : Boolean) {
        this.isEnabled = enabled
    }


}