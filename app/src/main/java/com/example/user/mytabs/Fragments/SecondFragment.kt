package com.example.user.mytabs.Fragments


import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.user.mytabs.Models.QuestionModel
import com.example.user.mytabs.Adapters.QuestionsSlidingAdapter
import com.example.user.mytabs.R
import kotlinx.android.synthetic.main.fragment_second.*
import kotlinx.android.synthetic.main.fragment_second.view.*
import java.util.*



// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

/**
 * A simple [Fragment] subclass.
 *
 */
class SecondFragment : Fragment() {

    private var qustionModelArrayList: ArrayList<QuestionModel>? = null

    private val myQuestionList = arrayListOf<String>(
        "How much is your budget?",
        "Family or Friends?",
        "Do you need a ride?",
        "Are you hungry?",
        "Cash or Credit Card?"
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

//        Anything after return doesn't get executed
//
        var myView =  inflater.inflate(R.layout.fragment_first, container, false)

        qustionModelArrayList = ArrayList()
        qustionModelArrayList = populateList()

        myView.mViewPager.adapter = QuestionsSlidingAdapter(
            activity!!.applicationContext,
            this.qustionModelArrayList!!
        )

        myView.mIndicator.setViewPager(mViewPager)
        val density = resources.displayMetrics.density

        myView.mIndicator.setRadius(5 * density)
        var NUM_PAGES = qustionModelArrayList!!.size
        var currentPage: Int = 0
        // Auto start of viewpager
        val handler = Handler()
        val Update = Runnable {
            if (currentPage == NUM_PAGES) {
                currentPage = 0
            }
            myView.mViewPager!!.setCurrentItem(currentPage++, true)
        }
        /*val swipeTimer = Timer()
        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(Update)
            }
        }, 5000, 5000)*/

        // Pager listener over indicator
        myView.mIndicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                currentPage = position

            }

            override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) {

            }

            override fun onPageScrollStateChanged(pos: Int) {
            }
        })
        // Inflate the layout for this fragment

        return myView
    }

    private fun populateList(): ArrayList<QuestionModel> {

        val list = ArrayList<QuestionModel>()

        for (question in myQuestionList) {
            val questionModel = QuestionModel(question)
            list.add(questionModel)
        }
        return list
    }

//

    private fun init() {



    }
}
